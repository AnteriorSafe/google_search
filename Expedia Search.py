from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

driver = webdriver.Chrome()
driver.get("https://www.expedia.com")

try:
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'app')))
except:
    driver.quit()

driver.find_element(By.LINK_TEXT, 'Packages').click()
driver.find_element(By.CSS_SELECTOR,'div.uitk-field.has-floatedLabel-label.has-icon.has-no-placeholder').click()
driver.find_element(By.ID, 'locator').send_keys("Melbourne")


