from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time

# Initializing the Driver
driver = webdriver.Chrome()
driver.maximize_window()
driver.get("https://www.google.com")
if not "Google" in driver.title:
    raise Exception("Unable to load google page!")
    
# Creating the Function for search
def google_search(result):
    search_box = driver.find_element_by_name('q')
    search_box.send_keys(f"{result}")
    search_box.submit()
    try:
        element = WebDriverWait(driver, 5).until(
            EC.presence_of_element_located((By.PARTIAL_LINK_TEXT, f"{result}"))
        )
    finally:
        driver.save_screenshot(f"./{result} .png")
    time.sleep(5)
    driver.back()

# Skip cookies consent
try:
    WebDriverWait(driver, 10).until(EC.frame_to_be_available_and_switch_to_it((By.CSS_SELECTOR, "iframe[src^='https://consent.google.com']")))
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//div[@id='introAgreeButton']"))).click()
except:
    pass

# Search in google
google_search('bahamas')
# Second search in google
google_search('amsterdam')
driver.quit()

